// GENERATED AUTOMATICALLY FROM 'Assets/TAREAS_PEC/Controles/Inputs_Custom.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Inputs_Custom : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Inputs_Custom()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Inputs_Custom"",
    ""maps"": [
        {
            ""name"": ""Tank0"",
            ""id"": ""4232dc4a-7697-4d1d-b073-b31b9e638b33"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""13000250-bec9-4012-a9fa-6e8f73a0677a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""00e43c91-8922-45f7-b545-3110c90fb115"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AltFire"",
                    ""type"": ""Button"",
                    ""id"": ""5b595855-a1f7-47e0-b129-78a88c9e83ff"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""64781190-287c-4655-a3c4-44356995acf0"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""right"",
                    ""id"": ""31f6e488-6eaf-45f9-9efb-b7813a1e5343"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c02d88a6-2aa4-4d9e-abfa-35d9a2ef893a"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""51547428-3b52-4c92-aed6-fd5b18ba4b36"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""up"",
                    ""id"": ""da473a73-d5a9-4561-b5b3-9062fa2bb0ff"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b5f00e30-11b3-4722-b188-785b84df6678"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""35687bde-cd9c-4142-989a-59d9e59eb671"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""AltFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Tank1"",
            ""id"": ""ff32ba11-8e86-45eb-973c-049c95ae93fb"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""f7bf163c-59a7-4e2a-a9ee-0833cfb9ae3a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""e88bc443-9eee-4c1d-84c4-29f92535b95a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AltFire"",
                    ""type"": ""Button"",
                    ""id"": ""fd9a7b38-78ce-453e-b10b-4b74e903a089"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""4e1b7926-d913-423a-988e-289420aa5ee0"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""right"",
                    ""id"": ""ad404d57-e87c-4636-8985-e2f46b70a552"",
                    ""path"": ""<Keyboard>/numpad8"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6a12090c-f4b2-4508-bf16-f6c8c342e4fe"",
                    ""path"": ""<Keyboard>/numpad5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c64296ce-13f5-4937-a2e7-25988b8ef69b"",
                    ""path"": ""<Keyboard>/numpad4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""up"",
                    ""id"": ""f0265e70-391b-493d-9648-a836507433df"",
                    ""path"": ""<Keyboard>/numpad6"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ba169720-4349-417a-b6e1-19968b0f6e79"",
                    ""path"": ""<Keyboard>/numpad0"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d8d9cb53-f745-41c6-bd5b-ab62b1cd3066"",
                    ""path"": ""<Keyboard>/numpad1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""AltFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Tank2"",
            ""id"": ""655b45c0-2d0f-4b75-b2e8-266755471835"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""e423f2a0-c2b7-4ab3-a9e8-560c653a3938"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""903ad524-9517-4396-bf8b-eaf540933a96"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AltFire"",
                    ""type"": ""Button"",
                    ""id"": ""5a628efa-c0b1-4e70-a111-63d36702c905"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""44db981b-ad50-491d-b34d-9702a139aca5"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""right"",
                    ""id"": ""058bec6e-1f5d-431a-b591-06bf9bb8ef97"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0d1f451e-ee47-4c51-a0ee-c8a643ef8a63"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""82b17ab7-910e-474a-8249-3cf37afdd444"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""up"",
                    ""id"": ""1f6d96db-7edb-464d-a7e9-4fda3359dccd"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""e5e626aa-fac9-48a9-a7b9-5fb63901f07b"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8f591df7-bb07-4b4d-a484-0a2701a31945"",
                    ""path"": ""<Keyboard>/rightShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""AltFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Tank3"",
            ""id"": ""333a63ba-9256-4af2-9500-3232f75b7e27"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""b377535c-7d14-4113-838d-e17ff2b80749"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""a2515604-dd1a-4e73-ac1f-4cf09c81980d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AltFire"",
                    ""type"": ""Button"",
                    ""id"": ""6ebb4b49-d725-4c3f-bbc9-ae8fdfc99283"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Stick"",
                    ""id"": ""51622830-1b8e-42c1-a18f-2fa4c2c7abba"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a5474c8f-b1b0-4644-a7ed-4906bda7a9ef"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""85130754-b072-49a5-8be8-7b85e5052e3e"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""692c6c66-02fd-4649-99ad-6b358f9aa490"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""up"",
                    ""id"": ""0af55465-e566-4afa-8901-3ebcc3f95107"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""973149dc-580d-4f6f-9cf9-3fb20266b21d"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e83f8a23-450e-42f3-a70d-ed53a06b6f72"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""ControlScheme_1"",
                    ""action"": ""AltFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""ControlScheme_1"",
            ""bindingGroup"": ""ControlScheme_1"",
            ""devices"": []
        }
    ]
}");
        // Tank0
        m_Tank0 = asset.FindActionMap("Tank0", throwIfNotFound: true);
        m_Tank0_Movement = m_Tank0.FindAction("Movement", throwIfNotFound: true);
        m_Tank0_Fire = m_Tank0.FindAction("Fire", throwIfNotFound: true);
        m_Tank0_AltFire = m_Tank0.FindAction("AltFire", throwIfNotFound: true);
        // Tank1
        m_Tank1 = asset.FindActionMap("Tank1", throwIfNotFound: true);
        m_Tank1_Movement = m_Tank1.FindAction("Movement", throwIfNotFound: true);
        m_Tank1_Fire = m_Tank1.FindAction("Fire", throwIfNotFound: true);
        m_Tank1_AltFire = m_Tank1.FindAction("AltFire", throwIfNotFound: true);
        // Tank2
        m_Tank2 = asset.FindActionMap("Tank2", throwIfNotFound: true);
        m_Tank2_Movement = m_Tank2.FindAction("Movement", throwIfNotFound: true);
        m_Tank2_Fire = m_Tank2.FindAction("Fire", throwIfNotFound: true);
        m_Tank2_AltFire = m_Tank2.FindAction("AltFire", throwIfNotFound: true);
        // Tank3
        m_Tank3 = asset.FindActionMap("Tank3", throwIfNotFound: true);
        m_Tank3_Movement = m_Tank3.FindAction("Movement", throwIfNotFound: true);
        m_Tank3_Fire = m_Tank3.FindAction("Fire", throwIfNotFound: true);
        m_Tank3_AltFire = m_Tank3.FindAction("AltFire", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Tank0
    private readonly InputActionMap m_Tank0;
    private ITank0Actions m_Tank0ActionsCallbackInterface;
    private readonly InputAction m_Tank0_Movement;
    private readonly InputAction m_Tank0_Fire;
    private readonly InputAction m_Tank0_AltFire;
    public struct Tank0Actions
    {
        private @Inputs_Custom m_Wrapper;
        public Tank0Actions(@Inputs_Custom wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Tank0_Movement;
        public InputAction @Fire => m_Wrapper.m_Tank0_Fire;
        public InputAction @AltFire => m_Wrapper.m_Tank0_AltFire;
        public InputActionMap Get() { return m_Wrapper.m_Tank0; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Tank0Actions set) { return set.Get(); }
        public void SetCallbacks(ITank0Actions instance)
        {
            if (m_Wrapper.m_Tank0ActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnMovement;
                @Fire.started -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnFire;
                @AltFire.started -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnAltFire;
                @AltFire.performed -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnAltFire;
                @AltFire.canceled -= m_Wrapper.m_Tank0ActionsCallbackInterface.OnAltFire;
            }
            m_Wrapper.m_Tank0ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @AltFire.started += instance.OnAltFire;
                @AltFire.performed += instance.OnAltFire;
                @AltFire.canceled += instance.OnAltFire;
            }
        }
    }
    public Tank0Actions @Tank0 => new Tank0Actions(this);

    // Tank1
    private readonly InputActionMap m_Tank1;
    private ITank1Actions m_Tank1ActionsCallbackInterface;
    private readonly InputAction m_Tank1_Movement;
    private readonly InputAction m_Tank1_Fire;
    private readonly InputAction m_Tank1_AltFire;
    public struct Tank1Actions
    {
        private @Inputs_Custom m_Wrapper;
        public Tank1Actions(@Inputs_Custom wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Tank1_Movement;
        public InputAction @Fire => m_Wrapper.m_Tank1_Fire;
        public InputAction @AltFire => m_Wrapper.m_Tank1_AltFire;
        public InputActionMap Get() { return m_Wrapper.m_Tank1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Tank1Actions set) { return set.Get(); }
        public void SetCallbacks(ITank1Actions instance)
        {
            if (m_Wrapper.m_Tank1ActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnMovement;
                @Fire.started -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnFire;
                @AltFire.started -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnAltFire;
                @AltFire.performed -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnAltFire;
                @AltFire.canceled -= m_Wrapper.m_Tank1ActionsCallbackInterface.OnAltFire;
            }
            m_Wrapper.m_Tank1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @AltFire.started += instance.OnAltFire;
                @AltFire.performed += instance.OnAltFire;
                @AltFire.canceled += instance.OnAltFire;
            }
        }
    }
    public Tank1Actions @Tank1 => new Tank1Actions(this);

    // Tank2
    private readonly InputActionMap m_Tank2;
    private ITank2Actions m_Tank2ActionsCallbackInterface;
    private readonly InputAction m_Tank2_Movement;
    private readonly InputAction m_Tank2_Fire;
    private readonly InputAction m_Tank2_AltFire;
    public struct Tank2Actions
    {
        private @Inputs_Custom m_Wrapper;
        public Tank2Actions(@Inputs_Custom wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Tank2_Movement;
        public InputAction @Fire => m_Wrapper.m_Tank2_Fire;
        public InputAction @AltFire => m_Wrapper.m_Tank2_AltFire;
        public InputActionMap Get() { return m_Wrapper.m_Tank2; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Tank2Actions set) { return set.Get(); }
        public void SetCallbacks(ITank2Actions instance)
        {
            if (m_Wrapper.m_Tank2ActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnMovement;
                @Fire.started -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnFire;
                @AltFire.started -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnAltFire;
                @AltFire.performed -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnAltFire;
                @AltFire.canceled -= m_Wrapper.m_Tank2ActionsCallbackInterface.OnAltFire;
            }
            m_Wrapper.m_Tank2ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @AltFire.started += instance.OnAltFire;
                @AltFire.performed += instance.OnAltFire;
                @AltFire.canceled += instance.OnAltFire;
            }
        }
    }
    public Tank2Actions @Tank2 => new Tank2Actions(this);

    // Tank3
    private readonly InputActionMap m_Tank3;
    private ITank3Actions m_Tank3ActionsCallbackInterface;
    private readonly InputAction m_Tank3_Movement;
    private readonly InputAction m_Tank3_Fire;
    private readonly InputAction m_Tank3_AltFire;
    public struct Tank3Actions
    {
        private @Inputs_Custom m_Wrapper;
        public Tank3Actions(@Inputs_Custom wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Tank3_Movement;
        public InputAction @Fire => m_Wrapper.m_Tank3_Fire;
        public InputAction @AltFire => m_Wrapper.m_Tank3_AltFire;
        public InputActionMap Get() { return m_Wrapper.m_Tank3; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Tank3Actions set) { return set.Get(); }
        public void SetCallbacks(ITank3Actions instance)
        {
            if (m_Wrapper.m_Tank3ActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnMovement;
                @Fire.started -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnFire;
                @AltFire.started -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnAltFire;
                @AltFire.performed -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnAltFire;
                @AltFire.canceled -= m_Wrapper.m_Tank3ActionsCallbackInterface.OnAltFire;
            }
            m_Wrapper.m_Tank3ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @AltFire.started += instance.OnAltFire;
                @AltFire.performed += instance.OnAltFire;
                @AltFire.canceled += instance.OnAltFire;
            }
        }
    }
    public Tank3Actions @Tank3 => new Tank3Actions(this);
    private int m_ControlScheme_1SchemeIndex = -1;
    public InputControlScheme ControlScheme_1Scheme
    {
        get
        {
            if (m_ControlScheme_1SchemeIndex == -1) m_ControlScheme_1SchemeIndex = asset.FindControlSchemeIndex("ControlScheme_1");
            return asset.controlSchemes[m_ControlScheme_1SchemeIndex];
        }
    }
    public interface ITank0Actions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnAltFire(InputAction.CallbackContext context);
    }
    public interface ITank1Actions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnAltFire(InputAction.CallbackContext context);
    }
    public interface ITank2Actions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnAltFire(InputAction.CallbackContext context);
    }
    public interface ITank3Actions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnAltFire(InputAction.CallbackContext context);
    }
}
