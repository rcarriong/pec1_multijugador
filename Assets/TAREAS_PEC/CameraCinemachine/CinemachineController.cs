﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineController : MonoBehaviour
{

public Animator cinemacineAnimator;

public void ChangeState(int idState){
    cinemacineAnimator.SetInteger("Camera", idState);
}

}
