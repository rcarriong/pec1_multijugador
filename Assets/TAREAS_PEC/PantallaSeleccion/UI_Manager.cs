﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    public Complete.SO_SelectionScreen so_data;
    

    public void NumberOfPlayers(int playersCount){
        so_data.numberOfPlayersAtStart = playersCount;
    }

    public void StartGame(){
        SceneManager.LoadScene("_Complete-Game");
    }

    public void QuitGame(){
        Debug.Log("ExitGame");
    Application.Quit();

    }


}
