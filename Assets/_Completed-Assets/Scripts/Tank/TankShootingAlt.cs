﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace Complete
{
    public class TankShootingAlt : MonoBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify the different players.
        public Rigidbody m_Shell;                   // Prefab of the shell.
        public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
        public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
        public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
        public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
        public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
        public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
        public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
        public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.


        private string m_FireButton;                // The input axis that is used for launching shells.
        private string m_alternativeFire;           // ***********boton alternativo
        private bool isAlternativeFire;             //si es alternativo que lleve a cabo el color y todo eso
        public Material redMaterial;
        private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
        private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
        private bool m_Fired;                       // Whether or not the shell has been launched with this button press.

        [Header("New input system")]
        private Inputs_Custom inpCustom;
        public InputActionMap inpACTSfire;
        private int fireInt;
        private bool isCharging;
        public SO_SelectionScreen so_data;

        private void Awake() {
            inpCustom = new Inputs_Custom();
            isCharging=false;
            //GameObject aimContainer = GameObject.Find("AimSlider");
            //m_AimSlider = aimContainer.GetComponent<UnityEngine.UI.Slider>();
            //m_AimSlider = so_data.aimSlider;
            
        }

        public void SetInputActionID(int id){

            switch(id){
                case 0:
                 inpACTSfire = inpCustom.Tank0;
                    break;
                case 1:
                 inpACTSfire = inpCustom.Tank1;
                    break;
                case 2:
                 inpACTSfire = inpCustom.Tank2;
                    break;
                case 3:
                 inpACTSfire = inpCustom.Tank3;
                    break;
            } 
            AwakeReplacement();
        } 
         public void AwakeReplacement(){                 //Lo pondría en el awake pero como lo llama antes de tener valor tengo que hacer esto a posteriori
            inpACTSfire["Fire"].performed += ctx => ChargeShoot(1);
            inpACTSfire["Fire"].canceled+= ctx => Fire();
            
            inpACTSfire["AltFire"].performed += ctx => ChargeShoot(2);
            inpACTSfire["AltFire"].canceled += ctx => Fire();
            
  
        }

        public void ChargeShoot(int id){
        if(id ==2) isAlternativeFire=true;
        else isAlternativeFire=false;
        m_CurrentLaunchForce= m_MinLaunchForce;
        isCharging=true;
        }
        
        private void OnEnable()
        {
            inpCustom.Enable();
            // When the tank is turned on, reset the launch force and the UI
            m_CurrentLaunchForce = m_MinLaunchForce;
            m_AimSlider.value = m_MinLaunchForce;
        }
        private void OnDisable() {
        inpCustom.Disable();    
        }

        private void Start ()
        {
            
            m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
        
        }

        private void Update ()
        {   
            if(isCharging){
            m_CurrentLaunchForce += m_ChargeSpeed*Time.deltaTime;
            m_AimSlider.value = m_CurrentLaunchForce;
            }
        }


        public void Fire ()
        {
            // Set the fired flag so only Fire is only called once.
            m_Fired = true;

            // Create an instance of the shell and store a reference to it's rigidbody.     //AQUI HAY UN PROBLEMA!
            Rigidbody shellInstance = new Rigidbody();
            shellInstance = Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation);
            // Set the shell's velocity to the launch force in the fire position's forward direction.

            if(isAlternativeFire){
            Renderer rendShell = shellInstance.GetComponent<Renderer>();
            rendShell.material.color = Color.red;
            shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward*1.5f;   //CAMBIO VELOCIDAD Y COLOR 
            }
            else
            {
            shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward; 
            }

            // Change the clip to the firing clip and play it.
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play ();

            // Reset the launch force.  This is a precaution in case of missing button events.
            m_AimSlider.value=m_MinLaunchForce;
            m_CurrentLaunchForce = m_MinLaunchForce;
            isAlternativeFire=false;
            isCharging =false;
        }
    }
}